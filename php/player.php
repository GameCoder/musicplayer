<script>

        $(document).ready(function(){
            currentPlaylist = <?php echo $json;?>;
            audioElement = new Audio();
            setTrack(currentPlaylist[0],currentPlaylist,true);
            updateVolumeProgressBar(audioElement.audio);

        
            $(".music-player-progressbar").mousedown(function(){
                mouseDown = true;
            });

            $(".music-player-progressbar").mousemove(function(e){
                if(mouseDown == true){
                    timeFromOffset(e,this);
                }
            });

            $(".music-player-progressbar").mouseup(function(e){
                timeFromOffset(e,this);
            });

            $(".volume").mousedown(function(){
                mouseDown = true;
            });

            $(".volume").mousemove(function(e){
                if(mouseDown == true){
                    
                    let percentage = e.offsetX / $(this).width();
                    if(percentage >= 0 && percentage <=1){
                         audioElement.audio.volume = percentage;
                    }    
                }
            });

            $(".volume").mouseup(function(e){
                let percentage = e.offsetX / $(this).width();
                audioElement.audio.volume = percentage;
                
            });

            $(document).mouseup(function(){
                mouseDown = false;
            })


        });

        function timeFromOffset(mouse, progressBar){
            let percentage = mouse.offsetX / $(progressBar).width() * 100;
            let seconds = audioElement.audio.duration * (percentage / 100);
            audioElement.setTime(seconds);

        }

        function prevSong() {
            if(audioElement.audio.currentTime >= 3 || currentIndex == 0){
                audioElement.setTime(0);
            }else{
                currentIndex--;
                setTrack(currentPlaylist[currentIndex],currentPlaylist,true);
            }
        }

        function nextSong() {
            if(repeat == true){
                audioElement.setTime(0);
                playSong();
                return;
            }

            if(currentIndex == currentPlaylist.length - 1){
                currentIndex = 0;
            }else{
                currentIndex++;
            }
            var trackToPlay = currentPlaylist[currentIndex];
            setTrack(trackToPlay,currentPlaylist,true);
        }

        function repeatSong() {
            repeat = !repeat;
        }

        function muteSong() {
            audioElement.audio.muted = !audioElement.audio.muted;
        }

        function setTrack(trackId,newPlaylist,play){
            currentIndex = currentPlaylist.indexOf(trackId);
            pauseSong();

            $.post(
                "js/ajax/getSongJson.php",
                {songId: trackId},
                function(data){

                    var track = JSON.parse(data);
                
                    $(".music-player-trackname").text(track.title);

                    console.log(track)
                    audioElement.setTrack(track);
                    playSong();
                });
            if(play){
                audioElement.play();
            }
        }


        function playSong(){
            if(audioElement.audio.currentTime == 0){
                $.post(
                 "js/ajax/updatePlays.php",
                {songId: audioElement.currentPlaying.id},
                );
            }else{
                console.log("donttt");
            }

            $(".controlButton.play").hide();
            $(".controlButton.pause").show();
            audioElement.play();
        }
        function pauseSong(){
            $(".controlButton.pause").hide();
            $(".controlButton.play").show();
            audioElement.pause();
        }
        
    </script>

    <div class="music-player">
        <div class="music-player-buttons">
            
        </div>
        <p class="music-player-trackname"></p>
        <div class="music-player-bar">   
            <p class="current"></p>
            <div class="music-player-progressbar">
                <div class="music-player-progressbar-inner"></div>
            </div>
            <p class="remaining"></p>
        </div>
    </div>