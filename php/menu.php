<div class="menu">
    <div class="menu-tabs">
       <div class="menu-tabs-tab">
           <img class="menu-tabs-tab-icon" src="lib/icons/home.png">
           <p class="menu-tabs-tab-text">HOME</p>
       </div>
        <div class="menu-tabs-tab">
            <img class="menu-tabs-tab-icon" src="lib/icons/search.png">
            <p class="menu-tabs-tab-text">SEARCH</p>
        </div>
    </div>
    <div class="menu-playlist">
        <p class="menu-playlist-title">PLAYLISTS</p>
        <?php
            // max 5 playlistu
        ?>
        <div class="menu-playlist-new">
            <img class="menu-playlist-new-icon" src="lib/icons/new.png">
            <p class="menu-playlist-new-text">Create new!</p>
        </div>
    </div>
    <div class="menu-profile">
        <img class="menu-profile-icon" src="">
        <div class="menu-profile-wrap">
            <p class="menu-profile-wrap-name">Adam Smetana</p>
            <a href="" class="menu-profile-wrap-redirect">My profile</a>
        </div>
    </div>
</div>
