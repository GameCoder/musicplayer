<?php 
    //require "lib/Db.php";
    //Db::connect("localhost","musicplayer","root","");
    $con = mysqli_connect("localhost", "root", "", "musicplayer");
    //$result = Db::queryAll("SELECT id FROM songs ORDER BY RAND() LIMIT 10");
    $query = mysqli_query($con,"SELECT * FROM songs ORDER BY RAND() LIMIT 10");
    $resultAr = array();
    while($row = mysqli_fetch_array($query)){
        array_push($resultAr,$row["id"]);
    }
    $json = json_encode($resultAr);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <script src="js/script.js"></script>
    <link href="lib/style.css" rel="stylesheet">
    <style>
        .progressbar{
            width:400px;
            height:20px;
            background-color:gray;
        }
        .inner{
            width:0px;
            height:20px;
            background-color:blue;
        }

        .volume{
            width:100px;
            height:20px;
            background-color:gray;
        }
        .vinner{
            width:0px;
            height:20px;
            background-color:red;
        }

    </style>
</head>
<body>
    <?php
        require "php/menu.php";
        require "php/player.php"
    ?>
    
    <div class="player">
    <button class="controlButton skip" onclick="prevSong()">Prev</button>
    <button class="controlButton play" onclick="playSong()">Play</button>
    <button class="controlButton pause" onclick="pauseSong()">Pause</button>
    <button class="controlButton skip" onclick="nextSong()">Next</button>
    <button class="controlButton repeat" onclick="repeatSong()">SongRepeat</button>
    <div class="progressbar">
        <div class=inner></div>
    </div>
    <button onclick="muteSong()">mute</button><br>
    Volume <div class="volume">
        <div class=vinner></div>
    </div>
    </div>
</body>
</html>