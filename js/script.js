var currentPlaylist = [];
var audioElement;
var mouseDown = false;
var currentIndex = 0;
var repeat = false;

function formatTime(second){
    let time = Math.round(second);
    let minutes = Math.floor(time/60);
    let seconds = time - (minutes*60);

    let zero;
    if(seconds < 10){
        zero = "0";
        
    }else{
        zero = "";
    }
    return minutes + ":" + zero + seconds;
    

}

function updateProgressBar(audio){
    $(".current").text(formatTime(audio.currentTime));
    $(".remaining").text(formatTime(audio.duration - audio.currentTime));
    var progress = audio.currentTime / audio.duration * 100;

    $(".music-player-progressbar-inner").css({"width" : progress + "%"});
}

function updateVolumeProgressBar(audio){
    var volume = audio.volume * 100;
    $(".vinner").css({"width" : volume + "%"});
}

function Audio(){
    this.currentPlaying;
    this.audio = document.createElement("audio");

    this.audio.addEventListener("ended",function () {
        nextSong();
    });
    this.audio.addEventListener("canplay", function(){
        $(".remaining").text(formatTime(this.duration));
       
    });

    this.audio.addEventListener("timeupdate",function(){
        if(this.duration){
            updateProgressBar(this);
        }
    });

    this.audio.addEventListener("volumechange", function(){
        updateVolumeProgressBar(this);
    });

    this.setTrack = function(track){
        this.currentPlaying = track;
        this.audio.src = track.path; //source se rovná toho kde to hodíme
    }
    this.play = function(){
        this.audio.play();
    }
    this.pause = function(){
        this.audio.pause();
    }
    this.setTime = function(seconds){
        this.audio.currentTime = seconds;
    }
}